echo "generate new leetcode file..."

if ["$1" == ""]
then
    echo "Need file name"
    exit
fi

mkdir $1
cp example/main.go $1/
cd $1
go mod init $1
go mod tidy
code .
echo "generate new leetcode file success"