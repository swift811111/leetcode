package main

func main() {}

func TwoSum(numbers []int, target int) []int {
	if len(numbers) == 2 {
		return []int{1, 2}
	}
	start := 0
	end := len(numbers) - 1

	for {
		tn := numbers[start] + numbers[end]
		if tn == target {
			return []int{start + 1, end + 1}
		}
		if tn > target {
			end--
		} else {
			start++
		}
	}
}
