package main

func main() {}

func TwoSum(nums []int, target int) []int {

	m := make(map[int]int, len(nums))
	res := make([]int, 0, 2)
	for key, value := range nums {
		if idx, ok := m[value]; ok {
			res = append(res, idx, key)
			return res
		}
		m[target-value] = key
	}

	return res
}
